THÔNG TIN VÀ NHẬT KÝ PHÁT TRIỂN TÍNH NĂNG CHO TỦ ĐIỆN
====

Nội dung file này sẽ liên tục cập nhật về tính năng của tủ điện. Tính năng đang có và tính năng sắp với có gồm thời gian cụ thể.

----

# SƠ ĐỒ TỔNG QUÁT
![cabinet_demo.png](./Img/cabinet_demo.png)
*Hình 1. Sơ đồ tổng quát tủ điện, cloud, app*

# TÙY CHỌN TỦ ĐIỆN
Vì định hướng sắp tới mình sẽ hạn chế làm theo từng giải pháp riêng để tiện cho việc bảo trì khi bán rộng rãi nên việc tùy chọn cho từng khách hàng riêng giới hạn ở một số mục: 

- Số tải: Tối đa là 8 tải / tủ điện.
- Thống số từng tải: Sẽ tư vấn riêng cho từng khách hàng để phù hợp với nhu cầu khách hàng.
- Về việc lắp đặt, đường dây điện: 
    - Khách hàng có thể mua tủ về tự lắp, mình sẽ cung cấp video hướng dẫn.
    - Khách hàng nhờ tư vấn: Mình sẽ tư vấn và đưa ra hướng lắp đặt để họ tự lắp đặt hoặc xuống lắp đặt cho họ.

- Biểu mẫu một số thông tin khảo sát trước khi thiết kế tủ điện: [Bấm vào đây](https://docs.google.com/forms/d/e/1FAIpQLSd0WjYAeRutFSQ-lxHNMVVsTY3uAeY5QdkHkAQNu9Kosmydzw/viewform?usp=sf_link)

- Danh sách thiết bị trong tủ điện (**không tự ý gửi thông tin chi tiết cho khách hàng**): [Bấm vào đây](https://drive.google.com/file/d/1SL6ZRA-4goXz8sv6aPcyp_Iokqa9Gsxd/view?usp=sharing)

# APP
App có thể sử dụng: 
- [Farmext things](https://play.google.com/store/apps/details?id=io.farmext.monitor&hl=en&gl=US) App này dành cho người dùng chỉ cần điều khiển tủ điện, theo dõi máy đo.
	
- [Farmext](https://play.google.com/store/apps/details?id=com.farmext&hl=en&gl=US) App gồm đầy đủ tính năng đã tích hợp cả app farmext things trong đó


# TÍNH NĂNG

## Tính năng đã có
- Điều khiển tải tại tủ có 3 chế độ: Bật thủ công, tắt thủ công và tự động. Ở chế độ tự động người dùng có thể điều khiển bật/tắt bằng app.
- Điều khiển thiết bị: Tốc độ điều khiển hiện tại rất nhanh, không còn chậm như trước.
- **Trên app tự động đồng bộ trạng thái với tủ điện nếu người dùng bật/tắt bằng công tắc tại tủ**.
- Hẹn giờ bật/tắt thiết bị.
- Tự động bật/tắt thiết bị theo các chỉ số máy đo envisor do người dùng cài đặt.
    >Tính năng này đã có nhưng hãy chờ máy đo ổn định hãy giới thiệu rộng rãi

## Tính năng sắp có

#### Phiên bản 2
- Cảnh báo bằng sms/gọi điện đến sđt cài đặt trên app khi thiết bị offline quá 5 phút, điều khiển 

#### Phiên bản 3
- Đo điện các tải được sử dụng trên tủ
- Cảnh báo mất điện, quá dòng thiết bị bằng sms/gọi điện đến sđt cài đặt trên app
- Thống kê số điện tiêu thụ, theo dõi bằng biểu đồ trên app

# DỰ KIẾN THỜI GIAN RA MẮT CÁC PHIÊN BẢN
Cập nhật thường xuyên.

![cabinet_release.png](./Img/cabinet_release.png)
*Hình 2. Dự kiến thời gian ra mắt phiên bản*